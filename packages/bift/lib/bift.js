'use strict';


const ActionProvider = require('./action-provider');
const TokenizerProvider = require('./tokenizer-provider');



// Two factor tokenizing
module.exports = class Bift {



	/**
	 * Constructor
	 */
	constructor() {
		this._ap = new ActionProvider();
		this._tp = new TokenizerProvider();
	}




	/**
	 * Register a new tokenizer controller
	 *
	 * @param {String} name
	 * @param {Object} config
	 */
	tokenizer(name, config) {
		if (config) {
			this._tp.register(name, config);
			return this;
		}

		return this._tp.expect(name);
	}




	/**
	 * Registers a new action controller
	 *
	 * @param {String} id
	 * @param {Object} config
	 */
	action(id, config) {

		let tokenizerCtrl = this._tp.expect(config.tokenizer.name);

		let actionTokenizer = {
			encode(payload) {
				return tokenizerCtrl.encode(payload, config.tokenizer.options)
			},
			decode(token) {
				return tokenizerCtrl.decode(token, config.tokenizer.options)
			},
			destroy(token) {
				return tokenizerCtrl.destroy(token, config.tokenizer.options)
			}
		};

		this._ap.register(id, {
			...config,
			tokenizer: actionTokenizer
		});

		return this;
	}




	/**
	 * Helper to combine actionId and token to a base64 encoded string
	 * @param {String} actionId
	 * @param {String} token
	 */
	_encodeToken(actionId, token) {
		return Buffer.from(`${actionId}::${token}`).toString('base64');
	}




	/**
	 * Helper to decode a base64 encoded string to actionId and action-type-token
	 * @param {String} _token
	 */
	_decodeToken(_token) {
		let [actionId, token] = Buffer.from(_token, 'base64').toString('utf-8').split('::');
		return {
			actionId,
			token
		}
	}




	/**
	 * Tokenize a payload to start a new transaction
	 * @param {String} actionId
	 * @param {Object} payload
	 */
	async init(actionId, payload) {
		let actionToken = await this._ap.init(actionId, payload);
		return Buffer.from(`${actionId}::${actionToken}`).toString('base64');
	}




	/**
	 * Load state behind token to enter transaction again
	 * @param {String} _token
	 */
	async preflight(_token) {
		let { actionId, token } = this._decodeToken(_token);

		return {
			action: actionId,
			data: await this._ap.preflight(actionId, token)
		}
	}




	/**
	 * Finalize transaction by given token and payload
	 * @param {String} _token
	 * @param {Object} payload
	 */
	async redeem(_token, payload) {
		let { actionId, token } = this._decodeToken(_token);
		return this._ap.redeem(actionId, token, payload);
	}




}
