'use strict';

const TokenizerCtrl = require('./tokenizer-ctrl');




module.exports = class TokenizerProvider {




	/**
	 * Constructor
	 */
	constructor() {
		this._tokenizers = new Map();
	}




	/**
	 * Register new tokenizer schema
	 *
	 * @param {String} name
	 * @param {Object} config
	 */
	register(name, config) {
		if (this._tokenizers.has(name)) {
			throw new Error(`Tokenizer already registered: ${name}`);
		}
		let ctrl = new TokenizerCtrl(name, config);
		this._tokenizers.set(name, ctrl);

		return this;
	}




	/**
	 * Getter which throws error on unknown tokenizer
	 *
	 * @param {String} name
	 */
	expect(name) {
		if (!this._tokenizers.has(name)) {
			throw new Error(`Unknown tokenizer: ${name}`);
		}
		return this._tokenizers.get(name);
	}




}
