'use strict';

const Joi = require('joi');


const JOI_SCHEMA_CONFIG = Joi.object({

	encode: Joi.func().required(),
	decode: Joi.func().required(),
	destroy: Joi.func()

}).required();




module.exports = class TokenizerCtrl {



	/**
	 * Constructor
	 *
	 * @param {String} name
	 * @param {Object} _config
	 */
	constructor(name, _config) {
		this.name = name;
		this._config = Joi.attempt(_config, JOI_SCHEMA_CONFIG);
	}




	/**
	 * Uniform interface method to encode payload with given config to a new token
	 *
	 * @param {Object} payload
	 * @param {Object} config
	 */
	async encode(payload, config) {
		return this._config.encode(payload, config);
	}




	/**
	 * Uniform interface method to decode token with a given config to its payload representation
	 *
	 * @param {String} payload
	 * @param {Object} config
	 */
	async decode(token, config) {
		return this._config.decode(token, config);
	}



	/**
	 * Uniform interface method to destroy a token
	 *
	 * @param {String} token
	 */
	async destroy(token) {
		if (this._config.destroy) {
			await this._config.destroy.apply(null, [token]);
		}
	}

}
