'use strict';



const { expect } = require('code');

const TokenizerCtrl = require('./tokenizer-ctrl');

describe('TokenizerController', () => {



	describe('encode()', () => {

		it('should execute given encode function', async () => {

			let called = false;

			let tc = new TokenizerCtrl('test', {
				encode(payload, config) {
					expect(payload).to.equal('payload');
					expect(config).to.equal('config');
					called = true;
					return 'token';
				},
				decode() {

				}
			});

			let res = await tc.encode('payload', 'config');
			expect(res).to.equal('token');
			expect(called).to.be.true();

		});

	});



	describe('decode()', () => {

		it('should execute given decode function', async () => {

			let called = false;

			let tc = new TokenizerCtrl('test', {
				encode() {

				},
				decode(token, config) {
					expect(token).to.equal('token');
					expect(config).to.equal('config');
					called = true;
					return 'payload';
				},

			});

			let res = await tc.decode('token', 'config');
			expect(res).to.equal('payload');
			expect(called).to.be.true();

		});

	});



	describe('destroy()', () => {

		it('should execute given destroy function', async () => {

			let called = false;

			let tc = new TokenizerCtrl('test', {
				encode() {

				},
				decode() {

				},
				destroy(token) {
					expect(token).to.equal('token')
					called = true;
				}
			});

			await tc.destroy('token');
			expect(called).to.be.true();

		});



	});

});
