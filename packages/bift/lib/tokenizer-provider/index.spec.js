'use strict';


const { expect } = require('code');

const TokenizerProvider = require('./');
const TokenizerCtrl = require('./tokenizer-ctrl');



describe('TokenizerProvider', () => {

	describe('api', () => {

		it('should work', async () => {

			let tp = new TokenizerProvider();

			let res = tp.register('test', {
				encode() {

				},
				decode() {

				}
			});

			expect(res).to.shallow.equal(tp);

			let tokenizer = tp.expect('test');
			expect(tokenizer).to.be.instanceOf(TokenizerCtrl);
			expect(tokenizer.name).to.equal('test');

		});

	});

});
