'use strict';


const ActionProvider = require('./');
const { expect } = require('code');
const Joi = require('joi');


describe('ActionProvider', () => {

	describe('init, preflight, redeem', () => {

		it('should work on registered action-type like using ActionController directly', async () => {

			let ap = new ActionProvider();

			ap.register('test', {
				tokenizer: {
					encode(payload) {
						expect(payload).to.equal({ qwe: 123 });
						return 'token';
					},
					decode(token) {
						expect(token).to.equal('token');
						return 'payload';
					}
				},
				redeem(ticket, payload) {
					expect(ticket).to.equal('payload');
					return payload;
				}
			});


			let token = await ap.init('test', { qwe: 123 });
			expect(token).to.equal('token');

			let preflightResult = await ap.preflight('test', token);
			expect(preflightResult).to.equal('payload');

			let redeemResult = await ap.redeem('test', token, 'ok');
			expect(redeemResult).to.equal('ok');

		});


	});



});
