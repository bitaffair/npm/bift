'use strict';


const ActionCtrl = require('./action-ctrl');
const { expect } = require('code');
const Joi = require('joi');



describe('ActionCtrl', () => {

	describe('init', () => {

		describe('configured as function', () => {

			it('should receive payload and tokenize return value', async () => {

				let initPayload = { qwe: 123 };

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					init(payload) {
						expect(payload).to.shallow.equal(initPayload);
						return 123;
					}
				});


				let res = await ac.init(initPayload);
				expect(res).to.equal(123);
			});


		});



		describe('configured as object', () => {

			it('should reject with payload validation error', async () => {

				let initPayload = { qwe: 123 };

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					init: {
						payloadSchema: Joi.object({
							asd: Joi.number().required()
						})
					}
				});


				await expect(ac.init(initPayload)).to.reject(Error);
			});


			it('should validate with payloadSchema', async () => {

				let initPayload = { qwe: 'QWE' };

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					init: {
						payloadSchema: Joi.object({
							qwe: Joi.string().lowercase().required()
						})
					}
				});


				let res = await ac.init(initPayload);
				expect(res.qwe).to.equal('qwe');

			});

			it('should run interceptor', async () => {

				let initPayload = { qwe: 'QWE' };

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					init: {
						async interceptor(payload) {
							expect(payload).to.shallow.equal(initPayload);
							return payload;
						}
					}
				});


				let res = await ac.init(initPayload);
				expect(res).to.shallow.equal(initPayload);

			});


			it('should run interceptor after schema validation', async () => {

				let initPayload = { qwe: 'QWE' };

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					init: {
						payloadSchema: Joi.object({
							qwe: Joi.string().lowercase().required()
						}),
						async interceptor(payload) {
							expect(payload).to.equal({ qwe: 'qwe' });
							return {
								qwe: 123
							};
						}
					}
				});


				let res = await ac.init(initPayload);
				expect(res).to.equal({ qwe: 123 });

			});


		});

	});



	describe('preflight', () => {

		describe('configured as function', () => {

			it('should run interceptor with ticket and ctx', async () => {

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					preflight(ticket, ctx) {
						expect(ticket).to.equal('payload');
						expect(ctx).to.equal('ctx');
						return 123;
					}
				});


				let token = await ac.init('payload');
				let res = await ac.preflight(token, 'ctx');
				expect(res).to.equal(123);
			});

		});

	});


	describe('redeem', () => {

		describe('configured as function', () => {

			it('should run interceptor with ticket and ctx', async () => {

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					redeem(ticket, redeemPayload) {
						expect(ticket).to.equal('payload');
						expect(redeemPayload).to.equal('redeemPayload');

						return 123;
					}
				});


				let token = await ac.init('payload');
				let res = await ac.redeem(token, 'redeemPayload');
				expect(res).to.equal(123);
			});

		});

		describe('configured as object', () => {

			it('without payloadSchema', async () => {

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					redeem: {
						handler(ticket, redeemPayload) {
							expect(ticket).to.equal('payload');
							expect(redeemPayload).to.equal('redeemPayload');

							return 123;
						}
					}
				});


				let token = await ac.init('payload');
				let res = await ac.redeem(token, 'redeemPayload');
				expect(res).to.equal(123);

			});


			it('should use given payloadSchema', async () => {

				let ac = new ActionCtrl('test', {
					tokenizer: { encode: (p) => p, decode: (t) => t },
					redeem: {
						payloadSchema: Joi.string().lowercase().required(),
						handler: (ticket, payload) => payload
					}
				});


				let token = await ac.init('payload');
				let res = await ac.redeem(token, 'REDEEM');
				expect(res).to.equal('redeem');

			});


		});

		it('should destroy token after redemption', async () => {

			let calledDestroy = false;

			let ac = new ActionCtrl('test', {
				tokenizer: {
					encode: (p) => 'token',
					decode: (t) => 'payload',
					async destroy(token, ticket, redeemPayload) {
						expect(token).to.equal('token');
						expect(ticket).to.equal('payload')
						expect(redeemPayload).to.equal('redeemPayload');
						calledDestroy = true;
					}
				}
			});


			let token = await ac.init('payload');
			await ac.redeem(token, 'redeemPayload');
			expect(calledDestroy).to.be.true();

		});

	});


});
