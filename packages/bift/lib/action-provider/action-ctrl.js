'use strict';


const Joi = require('joi');
const is = require('is');




/**
 * Constructor config schema
 */
const JOI_CONFIG_SCHEMA = Joi.object({

	tokenizer: Joi.object({
		encode: Joi.func().required(),
		decode: Joi.func().required(),
		destroy: Joi.func()
	}).required(),

	init: Joi.alternatives(
		Joi.func(),
		Joi.object({
			payloadSchema: Joi.object().schema(),
			interceptor: Joi.func().default((payload) => payload)
		})
	).default((payload) => payload),

	preflight: Joi.func().default(parent => parent.preflight),

	redeem: Joi.alternatives(
		Joi.func(),
		Joi.object({
			payloadSchema: Joi.object().schema(),
			handler: Joi.func().default((ticket) => ticket)
		})
	)

}).required();




/**
 * ActionCtrl
 *
 * Uniform interface for each action type.
 */
module.exports = class ActionCtrl {




	/**
	 * Constructor
	 *
	 * @param {String} name
	 * @param {Object} _config
	 */
	constructor(name, _config) {

		this.name = name;
		this._config = Joi.attempt(_config, JOI_CONFIG_SCHEMA);

	}




	/**
	 * Start a new transaction by tokenizing given payload
	 *
	 * @param {Object} payload
	 */
	async init(payload) {

		// validate and transform payload
		if (is.fn(this._config.init)) {
			payload = this._config.init(payload);
		} else if (is.object(this._config.init)) {
			if (is.object(this._config.init.payloadSchema)) {
				payload = Joi.attempt(payload, this._config.init.payloadSchema)
			}

			if (is.fn(this._config.init.interceptor)) {
				payload = await this._config.init.interceptor(payload);
			}
		}

		// tokenize payload
		let token = await this._config.tokenizer.encode(payload);

		return token;
	}




	/**
	 * Load current transaction state by given token
	 *
	 * @param {String} token
	 */
	async preflight(token, ctx) {
		let tokenPayload = await this._config.tokenizer.decode(token);

		return this._config.preflight?.apply(null, [tokenPayload, ctx]) || tokenPayload;
	}




	/**
	 * Finalize transaction
	 *
	 * @param {String} token
	 * @param {Object} payload
	 */
	async redeem(token, payload) {
		let tokenPayload = await this._config.tokenizer.decode(token);

		let handler;

		if (is.function(this._config.redeem)) {
			handler = this._config.redeem;
		} else if (is.object(this._config.redeem)) {
			if (is.object(this._config.redeem.payloadSchema)) {
				payload = Joi.attempt(payload, this._config.redeem.payloadSchema);
			}

			if (is.function(this._config.redeem.handler)) {
				handler = this._config.redeem.handler;
			}
		}

		let result = tokenPayload;

		if (handler) {
			result = await handler.apply(null, [tokenPayload, payload]);
		}

		// destroy token
		if (is.function(this._config.tokenizer.destroy)) {
			await this._config.tokenizer.destroy(token, tokenPayload, payload);
		}

		return result;
	}




}
