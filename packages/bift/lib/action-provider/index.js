'use strict';

const ActionCtrl = require('./action-ctrl');


module.exports = class ActionProvider {




	/**
	 * Constructor
	 */
	constructor() {
		this._actionTypes = new Map();
	}




	/**
	 * Registers a new action type
	 *
	 * @param {String} actionId
	 * @param {Object} config
	 */
	register(actionId, config) {
		if (this._actionTypes.has(actionId)) {
			throw new Error(`Action-Type already defined: ${actionId}`);
		}

		let ctrl = new ActionCtrl(actionId, config);
		this._actionTypes.set(actionId, ctrl);

		return this;
	}




	/**
	 * Getter which throws error in case of unknown actionId
	 *
	 * @param {String} actionId
	 */
	_expectActionCtrl(actionId) {
		if (!this._actionTypes.has(actionId)) {
			throw new Error(`Unknown Action-Type: ${actionId}`);
		}

		return this._actionTypes.get(actionId);
	}




	/**
	 * Init new transaction and get token to reenter it later
	 *
	 * @param {String} actionId
	 * @param {Object} payload
	 */
	async init(actionId, payload) {
		let ctrl = this._expectActionCtrl(actionId);
		return ctrl.init(payload);
	}




	/**
	 * Reenter transaction by given actionId and token to get current state
	 *
	 * @param {String} actionId
	 * @param {String} token
	 */
	async preflight(actionId, token) {
		let ctrl = this._expectActionCtrl(actionId);
		return ctrl.preflight(token);
	}




	/**
	 * Reenter transaction by given actionId and token and finalize it with given payload.
	 *
	 * @param {String} actionId
	 * @param {String} token
	 * @param {Object} payload
	 */
	async redeem(actionId, token, payload) {
		let ctrl = this._expectActionCtrl(actionId);
		return ctrl.redeem(token, payload);
	}




}
