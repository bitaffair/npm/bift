'use strict';




const { expect } = require('code');
const Bift = require('./bift');
const JwtTokenizer = require('@bitaffair/bift-jwt');
const Joi = require('joi');
const TokenizerCtrl = require('./tokenizer-provider/tokenizer-ctrl');



describe('Bift', () => {

	describe('api', () => {

		let bift;
		beforeEach(() => {
			bift = new Bift();
		});

		describe('tokenizer()', () => {

			it('should register tokenizer with given config and return self or return tokenizer instance without config', () => {

				let res = bift.tokenizer('jwt', JwtTokenizer({}));
				expect(res).to.shallow.equal(bift);

				let tokenizer = bift.tokenizer('jwt');
				expect(tokenizer).to.be.instanceOf(TokenizerCtrl);
				expect(tokenizer.name).to.equal('jwt');

			});

		});

		describe('action()', () => {

			it('should have method to register actions', () => {
				expect(bift.action).to.be.a.function();
			});

		});


		describe('init()', () => {

			it('should create Base64 encoded concatenated[actiontype, token] string as token', async () => {

				bift.tokenizer('jwt', JwtTokenizer({}));

				let res = bift.action('user-signup', {
					tokenizer: {
						name: 'jwt',
						options: {
							cert: 'OvaCXULkHcEWrzikAMVLPdVqV5ImqGDXHyiJsEFWkkXVaZGgR55uOtCWaHwqrQLS'
						}
					}
				});

				expect(res).to.shallow.equal(bift);

				let _token = await bift.init('user-signup', { email: 'mail@example.com' });
				expect(_token).to.be.a.string();

				let [actionId, token] = Buffer.from(_token, 'base64').toString('utf-8').split('::');

				expect(actionId)
					.to.be.a.string()
					.and.to.equal('user-signup');

				expect(token)
					.to.be.a.string();

			});

		});

	});

	describe('Flow', () => {

		it('should succeed', async () => {

			let bift = new Bift();

			bift.tokenizer('jwt', JwtTokenizer({}));


			bift.action('user-signup', {
				tokenizer: {
					name: 'jwt',
					options: {
						cert: 'rbv5dDra8iPehHvmztQW3zGAf0bejQQoj92j9ez1yYZE7VEbNiAFiigypf0NSuz1'
					}
				}
			});


			let token = await bift.init('user-signup', { email: 'mail@example.com' });
			expect(token).to.be.a.string();

			let preflightData = await bift.preflight(token);
			expect(preflightData).to.include(['action', 'data']);
			expect(preflightData.action).to.equal('user-signup');
			expect(preflightData.data.email).to.equal('mail@example.com');


			let redemptionData = await bift.redeem(token, {});
			expect(redemptionData.email).to.equal('mail@example.com');


		});



	});




})
