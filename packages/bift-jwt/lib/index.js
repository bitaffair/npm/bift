'use strict';

const JWT = require('jsonwebtoken');
const Joi = require('joi');
const { applyToDefaults } = require('@hapi/hoek');



const JOI_SCHEMA_CONFIG = Joi.object({
	cert: Joi.string().min(64),
	encodeOpts: Joi.object()
}).required();


module.exports = function BiftJWTTokenizer(globalConfig = {}) {

	globalConfig = Joi.attempt(globalConfig, JOI_SCHEMA_CONFIG);

	return {
		encode(payload, localConfig) {
			localConfig = Joi.attempt(localConfig, JOI_SCHEMA_CONFIG);

			const { cert, encodeOpts } = applyToDefaults(globalConfig, localConfig);

			return JWT.sign(payload, cert, encodeOpts);
		},
		decode(token, localConfig) {
			localConfig = Joi.attempt(localConfig, JOI_SCHEMA_CONFIG);

			const { cert } = applyToDefaults(globalConfig, localConfig);

			return JWT.verify(token, cert);
		}
	}

}
