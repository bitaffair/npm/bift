'use strict';


const { expect } = require('code');
const init = require('..');


const CERT = 'ffgNopbIPk5IlujfQANrORbwAUpKSP7D9EolUFEASdMR9I0GUUNPMvmwILlho6OO';

describe('JWT tokenizer', () => {

	it('should encode and decode', () => {

		let tokenizer = init({});
		let config = { cert: CERT };

		let token = tokenizer.encode({ qwe: 123 }, config);

		let payload = tokenizer.decode(token, config);

		expect(payload.qwe).to.equal(123);

	});


	describe('config.decodeOpts', () => {

		it('should decode should fail on expired token', () => {

			let tokenizer = init({
				encodeOpts: {
					expiresIn: '0s'
				}
			});
			let config = { cert: CERT };

			let token = tokenizer.encode({ qwe: 123 }, config);

			expect(() => tokenizer.decode(token, config))
				.to.throw('jwt expired');

		});


	});


})
